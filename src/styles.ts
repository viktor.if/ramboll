import {StyleSheet, Dimensions} from 'react-native';

const {width} = Dimensions.get('screen');

export default StyleSheet.create({
  screen: {
    backgroundColor: '#fff',
    flex: 1,
  },
  list: {
    flexGrow: 1,
  },
  listItem: {
    backgroundColor: '#ffb29b',
    borderColor: '#46211a',
    borderRadius: 8,
    borderWidth: 1,
    marginHorizontal: 16,
    marginVertical: 6,
    paddingHorizontal: 16,
    paddingVertical: 12,
    width: width - 32,
  },
  titleText: {
    color: '#000',
    fontSize: 18,
    fontWeight: '600',
  },
  bodyText: {
    color: '#000',
    fontSize: 16,
    fontWeight: '400',
  },
  timeText: {
    color: '#46211a',
    flex: 1,
    flexDirection: 'row',
    fontSize: 14,
    fontWeight: '400',
    textAlign: 'right',
    marginTop: 12,
  },
  emptyListText: {
    color: '#000',
    fontSize: 24,
    fontWeight: '600',
    marginTop: 50,
    textAlign: 'center',
  },
});
