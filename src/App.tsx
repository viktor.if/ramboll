import React, {useEffect, useState} from 'react';
import moment from 'moment';
import {FirebaseMessagingTypes} from '@react-native-firebase/messaging';

import {
  SafeAreaView,
  StatusBar,
  Platform,
  FlatList,
  View,
  Text,
} from 'react-native';

import styles from './styles';
import {TNotificationMessage} from './utils/types';
import {AsyncStorageService} from './utils/AsyncStorageService';
import {FCMService} from './utils/FCMService';

const STORAGE_KEY = '@messages';

FCMService.setBackgroundMessageHandler();

export default () => {
  const [messages, setMessages] = useState<TNotificationMessage[]>([]);

  // Receive a new message
  const onReceiveMessage = (msg: FirebaseMessagingTypes.RemoteMessage) => {
    setMessages(prevState => {
      const existsAlready = !!prevState.find(item => item.id === msg.messageId);

      if (existsAlready) {
        return prevState;
      }

      const nextState = [
        {
          id: msg.messageId ?? new Date().getTime().toString(),
          title: msg.notification?.title?.trim(),
          body: msg.notification?.body?.trim() ?? '',
          time: moment().format('HH:mm DD MMM'),
        },
        ...prevState,
      ];
      AsyncStorageService.save(STORAGE_KEY, nextState);
      return nextState;
    });
  };

  // Get data from the storage to the state
  useEffect(() => {
    AsyncStorageService.get(STORAGE_KEY, setMessages);
  }, []);

  useEffect(() => {
    const unsubscribe = FCMService.onMessage(onReceiveMessage);

    return unsubscribe;
  }, []);

  // Status bar
  useEffect(() => {
    StatusBar.setBarStyle('dark-content');

    if (Platform.OS === 'android') {
      StatusBar.setBackgroundColor('#fff');
      StatusBar.setTranslucent(false);
    }
  }, []);

  return (
    <SafeAreaView style={styles.screen}>
      <FlatList
        data={messages}
        keyExtractor={item => item.id}
        renderItem={({item}) => <ListItem {...item} />}
        contentContainerStyle={styles.list}
        ListEmptyComponent={
          <Text style={styles.emptyListText}>No messages</Text>
        }
      />
    </SafeAreaView>
  );
};

/** List item */
const ListItem = (message: TNotificationMessage): JSX.Element => (
  <View style={styles.listItem}>
    {!!message.title && (
      <Text style={styles.titleText} numberOfLines={1}>
        {message.title}
      </Text>
    )}
    <Text style={styles.bodyText} numberOfLines={5}>
      {message.body}
    </Text>
    <Text style={styles.timeText} numberOfLines={5}>
      {message.time}
    </Text>
  </View>
);
