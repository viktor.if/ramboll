import messaging, {
  FirebaseMessagingTypes,
} from '@react-native-firebase/messaging';

export class FCMService {
  // Get token
  static getToken = async () => {
    return await messaging().getToken();
  };

  // Request user permission
  static requestUserPermission = async () => {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
      console.log('Authorization status:', authStatus);
    }
  };

  // Handle received message in foreground
  static onMessage = (
    cb: (data: FirebaseMessagingTypes.RemoteMessage) => void,
  ) => {
    return messaging().onMessage(async remoteMessage => {
      console.log('A new FCM message arrived!', JSON.stringify(remoteMessage));
      cb(remoteMessage);
    });
  };

  /**
   * Set background message handler
   * Must be used outside of app
   */
  static setBackgroundMessageHandler = (
    cb?: (data: FirebaseMessagingTypes.RemoteMessage) => void,
  ) => {
    messaging().setBackgroundMessageHandler(async message => {
      cb?.(message);
      console.log(
        'Notification received in background: ',
        message.notification,
      );
    });
  };
}
