import AsyncStorage from '@react-native-async-storage/async-storage';

export class AsyncStorageService {
  // Save data to the local storage
  static save = async (key: string, data: any): Promise<void> => {
    try {
      await AsyncStorage.setItem(key, JSON.stringify(data));
    } catch (error) {
      console.warn(error);
    }
  };

  // Get data from the local storage
  static get = async (key: string, cb: (data: any) => void): Promise<void> => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value) {
        cb(JSON.parse(value));
      }
    } catch (error) {
      console.warn(error);
    }
  };
}
